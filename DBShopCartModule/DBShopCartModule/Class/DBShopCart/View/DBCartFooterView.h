//
//  DBCartFooterView.h
//  DBShopCartModule
//
//  Created by simon on 16/6/28.
//  Copyright © 2016年 DB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DBCartFooterView : UITableViewHeaderFooterView

@property (nonatomic, strong) NSMutableArray *shopGoodsArray;

+ (CGFloat)getCartFooterHeight;

@end
