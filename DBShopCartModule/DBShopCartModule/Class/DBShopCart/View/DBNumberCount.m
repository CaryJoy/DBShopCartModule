//
//  DBNumberCount.m
//  DBShopCartModule
//
//  Created by simon on 16/6/28.
//  Copyright © 2016年 DB. All rights reserved.
//

#import "DBNumberCount.h"

static CGFloat const Wd = 28;

@interface DBNumberCount ()
//加
@property (nonatomic, strong) UIButton      *addButton;
//减
@property (nonatomic, strong) UIButton      *subButton;
//数字输入框
@property (nonatomic, strong) UITextField   *numberTF;

@end

@implementation DBNumberCount

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setUI];
    }
    return self;
}

- (void)awakeFromNib {
    [self setUI];
}

#pragma mark - set UI
- (void)setUI {
    
    self.backgroundColor = [UIColor clearColor];
    self.currentCountNumber = 0;
    self.totalNum = 0;
    WEAK
    /****************************** 减 *********************************/
    _subButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _subButton.frame = CGRectMake(0, 0, Wd, Wd);
    [_subButton setBackgroundImage:[UIImage imageNamed:@"sub_Btn_normal"] forState:UIControlStateNormal];
    [_subButton setBackgroundImage:[UIImage imageNamed:@"sub_Btn_disable"] forState:UIControlStateDisabled];
    _subButton.tag = 7000;
    [[self.subButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        
       STRONG
        self.currentCountNumber--;
        if (self.numberChangeBlock) {
            self.numberChangeBlock(self.currentCountNumber);
        }
    }];
    [self addSubview:_subButton];
    
    /****************************** 内容 *********************************/
    self.numberTF = [[UITextField alloc] init];
    self.numberTF.frame = CGRectMake(CGRectGetMaxX(_subButton.frame), 0, Wd*1.5, _subButton.frame.size.height);
    self.numberTF.keyboardType = UIKeyboardTypeNumberPad;
    self.numberTF.text = [NSString stringWithFormat:@"%@", @(0)];
    self.numberTF.backgroundColor = [UIColor whiteColor];
    self.numberTF.textColor = [UIColor blackColor];
    self.numberTF.adjustsFontSizeToFitWidth = YES;
    self.numberTF.textAlignment = NSTextAlignmentCenter;
    self.numberTF.layer.borderColor = DBColor(201, 201, 201, 1).CGColor;
    self.numberTF.layer.borderWidth = 1.3;
    [self addSubview:self.numberTF];

    /****************************** 加 *********************************/
    _addButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _addButton.frame = CGRectMake(CGRectGetMaxX(_numberTF.frame), 0, Wd, Wd);
    [_addButton setBackgroundImage:[UIImage imageNamed:@"add_Btn_normal"] forState:UIControlStateNormal];
    [_addButton setBackgroundImage:[UIImage imageNamed:@"add_Btn_disable"] forState:UIControlStateDisabled];
    _addButton.tag = 7001;
    [[self.addButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        
       STRONG
        self.currentCountNumber++;
        if (self.numberChangeBlock) {
            self.numberChangeBlock(self.currentCountNumber);
        }
    }];
    [self addSubview:_addButton];
    
    /****************************** 内容改变 *********************************/
    [[[NSNotificationCenter defaultCenter] rac_addObserverForName:@"UITextFieldTextDidEndEditingNotification" object:self.numberTF] subscribeNext:^(id x) {
        STRONG
        UITextField *t1 = [x object];
        NSString *text = t1.text;
        NSInteger changeNum = 0;
        if (text.integerValue > self.totalNum && self.totalNum != 0) {
            
            self.currentCountNumber = self.totalNum;
            self.numberTF.text = [NSString stringWithFormat:@"%@", @(self.totalNum)];
            changeNum = self.totalNum;
            
        }else if (text.integerValue < 1) {
            
            self.numberTF.text = @"1";
            changeNum = 1;
            
        }else {
        
            self.currentCountNumber = text.integerValue;
            changeNum = self.currentCountNumber;
            
        }
        if (self.numberChangeBlock) {
            self.numberChangeBlock(changeNum);
        }
        
    }];
    
    /* 捆绑加减的enable */
    RACSignal *subSignal = [RACObserve(self, currentCountNumber) map:^id(NSNumber * subValue) {
        return @(subValue.integerValue > 1);
    }];
    RACSignal *addSignal = [RACObserve(self, currentCountNumber) map:^id(NSNumber * addValue) {
        return @(addValue.integerValue < self.totalNum);
    }];
    RAC(self.subButton, enabled) = subSignal;
    RAC(self.addButton, enabled) = addSignal;
    
    /* 内容颜色显示 */
    RACSignal *numColorSignal = [RACObserve(self, totalNum) map:^id(NSNumber * totalValue) {
        return totalValue.integerValue == 0 ? [UIColor redColor] : [UIColor blackColor];
    }];
    RAC(self.numberTF, textColor) = numColorSignal;
    
    /* 更新数字框内容 */
    RACSignal *textSignal = [RACObserve(self, currentCountNumber) map:^id(NSNumber * textValue) {
        return [NSString stringWithFormat:@"%@",textValue];
    }];
    RAC(self.numberTF, text) = textSignal;
}


@end
