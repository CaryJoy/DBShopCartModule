//
//  DBNumberCount.h
//  DBShopCartModule
//
//  Created by simon on 16/6/28.
//  Copyright © 2016年 DB. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^DBNumberChangeBlock)(NSInteger count);

@interface DBNumberCount : UIView

/**
 *  总数
 */
@property (nonatomic, assign) NSInteger totalNum;

/**
 *  当前显示的数量
 */
@property (nonatomic, assign) NSInteger currentCountNumber;

/**
 *  数量改变回调
 */
@property (nonatomic, copy) DBNumberChangeBlock numberChangeBlock;

@end
