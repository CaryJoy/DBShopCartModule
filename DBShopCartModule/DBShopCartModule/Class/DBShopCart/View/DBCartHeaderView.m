//
//  DBCartHeaderView.m
//  DBShopCartModule
//
//  Created by simon on 16/6/28.
//  Copyright © 2016年 DB. All rights reserved.
//

#import "DBCartHeaderView.h"

@interface DBCartHeaderView ()

@property (nonatomic, strong) UIButton *storeNameButton;

@end

@implementation DBCartHeaderView

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithReuseIdentifier:reuseIdentifier]) {
        
        [self setupHeaderUI];
    }
    return self;
}

- (void)setupHeaderUI {
    
    self.contentView.backgroundColor = [UIColor whiteColor];
    
    self.selectStoreGoodsButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.selectStoreGoodsButton.frame = CGRectZero;
    [self.selectStoreGoodsButton setImage:[UIImage imageNamed:@"btn_normal"] forState:UIControlStateNormal];
    [self.selectStoreGoodsButton setImage:[UIImage imageNamed:@"btn_green"] forState:UIControlStateSelected];
    self.selectStoreGoodsButton.backgroundColor = [UIColor clearColor];
    
    [self addSubview:self.selectStoreGoodsButton];
    
    /* 店名用Button是为了以后拓展功能 - 点击店名跳转店铺详情 */
    self.storeNameButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.storeNameButton.frame = CGRectZero;
    [self.storeNameButton setTitle:@"" forState:UIControlStateNormal];
    [self.storeNameButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    self.storeNameButton.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    self.storeNameButton.titleLabel.font = DBFont(13);
    self.storeNameButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [self addSubview:self.storeNameButton];
}

- (void)layoutSubviews {
    
    [super layoutSubviews];
    
    self.selectStoreGoodsButton.frame = CGRectMake(0, 0, 36, 30);
    
    self.storeNameButton.frame = CGRectMake(40, 0, DBWindowWidth - 40, 30);
}

+ (CGFloat)getCartHeaderHeight {
   
    return 30;
}

- (void)setShopName:(NSString *)shopName {
    _shopName = shopName;
    [self.storeNameButton setTitle:shopName forState:UIControlStateNormal];
}



@end
