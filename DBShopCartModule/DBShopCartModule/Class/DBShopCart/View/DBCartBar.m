//
//  DBCartBar.m
//  DBShopCartModule
//
//  Created by simon on 16/6/28.
//  Copyright © 2016年 DB. All rights reserved.
//

static NSInteger const BalanceButtonTag = 120;

static NSInteger const DeleteButtonTag =  121;

static NSInteger const SelectButtonTag = 122;

#import "DBCartBar.h"

@interface UIImage (DB)

+ (UIImage *)imageWithColor:(UIColor *)color;

@end

@implementation UIImage (DB)

+ (UIImage *)imageWithColor:(UIColor *)color {
    
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    return image;
}

@end

@implementation DBCartBar

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setupUI];
    }
    return self;
}


- (void)setupUI {
    
    self.backgroundColor = [UIColor clearColor];
    
    /* background */
    UIVisualEffectView *effectView = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleLight]];
    effectView.userInteractionEnabled = NO;
    effectView.frame = self.bounds;
    [self addSubview:effectView];
    
    CGFloat wd = DBWindowWidth * 2 / 7;
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DBWindowWidth, 0.5)];
    lineView.backgroundColor = DBColor(210, 210, 210, 1);
    [self addSubview:lineView];
    
    /* account */
    UIButton *balanceButton = [UIButton buttonWithType:UIButtonTypeCustom];
    balanceButton.frame = CGRectMake(DBWindowWidth - wd, 0, wd, self.frame.size.height);
    [balanceButton setBackgroundImage:[UIImage imageWithColor:[UIColor redColor]] forState:UIControlStateNormal];
    [balanceButton setBackgroundImage:[UIImage imageWithColor:[UIColor lightGrayColor]] forState:UIControlStateDisabled];
    [balanceButton setTitle:@"结算" forState:UIControlStateNormal];
    [balanceButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [balanceButton setTitleColor:[UIColor whiteColor] forState:UIControlStateDisabled];
    balanceButton.enabled = NO;
    balanceButton.tag = BalanceButtonTag;
    [self addSubview:balanceButton];
    _balanceButton = balanceButton;
    
    /* delete */
    UIButton *deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    deleteButton.frame = CGRectMake(DBWindowWidth - wd, 0, wd, self.frame.size.height);
    [deleteButton setImage:[UIImage imageWithColor:[UIColor redColor]] forState:UIControlStateNormal];
    [deleteButton setImage:[UIImage imageWithColor:[UIColor lightGrayColor]] forState:UIControlStateDisabled];
    [deleteButton setTitle:@"删除" forState:UIControlStateNormal];
    [deleteButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [deleteButton setTitleColor:[UIColor whiteColor] forState:UIControlStateDisabled];
    deleteButton.enabled = NO;
    deleteButton.hidden = YES;
    deleteButton.tag = DeleteButtonTag;
    [self addSubview:deleteButton];
    _deleteButton = deleteButton;
    
    /* allSelete */
    UIButton *selectAllButton = [UIButton buttonWithType:UIButtonTypeCustom];
    selectAllButton.frame = CGRectMake(0, 0, 78, self.frame.size.height);
    [selectAllButton setImage:[UIImage imageNamed:@"btn_normal"] forState:UIControlStateNormal];
    [selectAllButton setImage:[UIImage imageNamed:@"btn_green"] forState:UIControlStateSelected];
    [selectAllButton setTitle:@"全选" forState:UIControlStateNormal];
    [selectAllButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [selectAllButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 0)];
    selectAllButton.tag = SelectButtonTag;
    [self addSubview:selectAllButton];
    _selectAllButton = selectAllButton;
    
    /* price */
    UILabel *priceLabel = [[UILabel alloc] initWithFrame:CGRectMake(wd, 0, DBWindowWidth - wd * 2 - 5, self.frame.size.height)];
    priceLabel.text = [NSString stringWithFormat:@"总计:%@",@(0.00)];
    priceLabel.textColor = [UIColor blackColor];
    priceLabel.font = DBFont(15);
    priceLabel.textAlignment = NSTextAlignmentRight;
    [self addSubview:priceLabel];
    _allMoneyLabel = priceLabel;
    
    /* assign value */
    WEAK
    [RACObserve(self, money) subscribeNext:^(NSNumber *x) {
        STRONG
        self.allMoneyLabel.text = [NSString stringWithFormat:@"总计:%.2f",x.floatValue];
    }];
    
    /* RAC BLIND */
    RAC(self.balanceButton, enabled) = [RACSignal combineLatest:@[RACObserve(self.selectAllButton, selected), RACObserve(self, money)] reduce:^id(NSNumber *isSelect, NSNumber *money){
        return @(isSelect.boolValue || money.floatValue > 0);
    }];
    
}

@end
