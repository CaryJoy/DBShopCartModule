//
//  DBCartCallTableViewCell.h
//  DBShopCartModule
//
//  Created by simon on 16/6/28.
//  Copyright © 2016年 DB. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DBCartModel, DBNumberCount;

@interface DBCartCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *selectShopGoodsButton;

@property (weak, nonatomic) IBOutlet DBNumberCount *numberCount;

@property (nonatomic, strong) DBCartModel *model;

+ (CGFloat)getCartCellHeight;

@end
