//
//  DBCartBar.h
//  DBShopCartModule
//
//  Created by simon on 16/6/28.
//  Copyright © 2016年 DB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DBCartBar : UIView

//结算
@property (nonatomic, strong) UIButton *balanceButton;
//全选
@property (nonatomic, strong) UIButton *selectAllButton;
//价格
@property (nonatomic, strong) UILabel *allMoneyLabel;
//删除
@property (nonatomic, strong) UIButton *deleteButton;

@property (nonatomic, assign) float money;


@end
