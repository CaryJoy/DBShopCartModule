//
//  DBCartHeaderView.h
//  DBShopCartModule
//
//  Created by simon on 16/6/28.
//  Copyright © 2016年 DB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DBCartHeaderView : UITableViewHeaderFooterView

@property (nonatomic, strong) UIButton *selectStoreGoodsButton;

@property (nonatomic, copy) NSString *shopName;

+ (CGFloat)getCartHeaderHeight;

@end
