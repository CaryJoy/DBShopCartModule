//
//  DBCartFooterView.m
//  DBShopCartModule
//
//  Created by simon on 16/6/28.
//  Copyright © 2016年 DB. All rights reserved.
//

#import "DBCartFooterView.h"
#import "DBCartModel.h"

@interface DBCartFooterView ()

@property (nonatomic, strong) UILabel *priceLabel;

@end

@implementation DBCartFooterView

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    
    if (self = [super initWithReuseIdentifier:reuseIdentifier]) {
        
        [self setupFooterUI];
    }
    return self;
}

- (void)setupFooterUI {
    
    self.contentView.backgroundColor = [UIColor whiteColor];
    
    _priceLabel = [[UILabel alloc] init];
    _priceLabel.textAlignment = NSTextAlignmentRight;
    _priceLabel.text = @"合记";
    _priceLabel.textColor = [UIColor redColor];
    
    [self addSubview:_priceLabel];
}

- (void)layoutSubviews {
    
    [super layoutSubviews];
    
    _priceLabel.frame = CGRectMake(10, 0.5, DBWindowWidth - 20, 30);
}

- (void)setShopGoodsArray:(NSMutableArray *)shopGoodsArray {
    
    _shopGoodsArray = shopGoodsArray;
    
    NSArray *pricesArray = [[[_shopGoodsArray rac_sequence] map:^id(DBCartModel *model) {
        return  @(model.p_quantity * model.p_price);
    }] array];
    
    float shopPrice = 0.f;
    
    for (NSNumber *prices in pricesArray) {
        shopPrice += prices.floatValue;
    }
    _priceLabel.text = [NSString stringWithFormat:@"小记:￥%.2f", shopPrice];
    
}

+ (CGFloat)getCartFooterHeight {
    return 30;
}

@end
