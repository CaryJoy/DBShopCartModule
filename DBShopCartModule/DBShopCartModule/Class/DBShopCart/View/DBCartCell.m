//
//  DBCartCallTableViewCell.m
//  DBShopCartModule
//
//  Created by simon on 16/6/28.
//  Copyright © 2016年 DB. All rights reserved.
//

#import "DBCartCell.h"
#import "DBNumberCount.h"
#import "DBCartModel.h"

@interface DBCartCell ()

@property (weak, nonatomic) IBOutlet UILabel *goodsNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *goodsStockLabel;
@property (weak, nonatomic) IBOutlet UILabel *goodsPricesLabel;
@property (weak, nonatomic) IBOutlet UIImageView *goodsImageView;

@end

@implementation DBCartCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(DBCartModel *)model {
    
    self.goodsNameLabel.text            = model.p_name;
    self.goodsStockLabel.text           = [NSString stringWithFormat:@"库存:%ld",model.p_stock];
    self.goodsPricesLabel.text          = [NSString stringWithFormat:@"￥:%.2f", model.p_price];
    self.numberCount.totalNum           = model.p_stock;
    self.numberCount.currentCountNumber = model.p_quantity;
    self.selectShopGoodsButton.selected = model.isSelect;
    
//    [self.goodsImageView ]
}

+ (CGFloat)getCartCellHeight {
    return 80;
}

@end
