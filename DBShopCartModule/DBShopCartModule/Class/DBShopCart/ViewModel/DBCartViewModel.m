//
//  DBCartViewModel.m
//  DBShopCartModule
//
//  Created by simon on 16/6/28.
//  Copyright © 2016年 DB. All rights reserved.
//

#import "DBCartViewModel.h"
#import "DBCartModel.h"

@interface DBCartViewModel ()
{
    NSArray *_shopGoodsCount;
    NSArray *_goodsPicArray;
    NSArray *_goodsPriceArray;
    NSArray *_goodsQuantityArray;
}
//随机获取店铺下商品数
@property (nonatomic, assign) NSInteger random;
@end

@implementation DBCartViewModel

- (instancetype)init {
    if (self = [super init]) {
        // a test
        _shopGoodsCount =@[@(1),@(2),@(4),@(4),@(5),@(6)];
//        _goodsPicArray
        _goodsPriceArray = @[@(32.1),@(11.23),@(33.2),@(12.63),@(32.32),@(12)];
        _goodsQuantityArray = @[@(23),@(21),@(14),@(5),@(23),@(53)];
    }
    return self;
}

- (NSInteger)random {
    NSInteger from = 0;
    NSInteger to   = 5;
    
    return (NSInteger)(from + (arc4random() % (to - from + 1)));
}

- (void)getData {
    //数据个数
    NSInteger allCount = 20;
    NSMutableArray *storeArray = [NSMutableArray arrayWithCapacity:allCount];
    NSMutableArray *shopSelectArray = [NSMutableArray arrayWithCapacity:allCount];
    
    //创造店铺数据
    for (int i = 0; i < allCount; i++) {
        //创造店铺下商品数据
        NSInteger goodsCount = [_shopGoodsCount[self.random] intValue];
        NSMutableArray *goodsArray = [NSMutableArray arrayWithCapacity:goodsCount];
        for (int x = 0; x < goodsCount; x++) {
            DBCartModel *cartModel = [[DBCartModel alloc] init];
            cartModel.p_id       = @"123";
            cartModel.p_price    = [_goodsPriceArray[self.random] floatValue];
            cartModel.p_name     = [NSString stringWithFormat:@"%@名字", @(x)];
            cartModel.p_stock    = 22;
            cartModel.p_imageUrl = _goodsPicArray[self.random];
            cartModel.p_quantity = [_goodsQuantityArray[self.random] integerValue];
            [goodsArray addObject:cartModel];
        }
        [storeArray addObject:goodsArray];
        [shopSelectArray addObject:@(NO)];
    }
    self.cartData = storeArray;
    self.shopSelectArray = shopSelectArray;
}

- (float)getAllPrices {
    
    __block float allPrices = 0;
    NSInteger shopCount = self.cartData.count;
    NSInteger shopSelectCount = self.shopSelectArray.count;
    
    if (shopSelectCount == shopCount && shopCount != 0) {
        self.isSelectAll = YES;
    }
    
    NSArray *pricseArray = [[[self.cartData rac_sequence] map:^id(NSMutableArray *value) {
        return [[[value rac_sequence] filter:^BOOL(DBCartModel *model) {
            if (!model.isSelect) {
                self.isSelectAll = NO;
            }
            return model.isSelect;
        }] map:^id(DBCartModel *model) {
            return @(model.p_quantity * model.p_price);
        }];
    }] array];
    
    for (NSArray *priceA in pricseArray) {
        for (NSNumber *price in priceA) {
            allPrices += price.floatValue;
        }
    }
    
    return allPrices;
}

- (void)selectAll:(BOOL)isSelect {
    
    __block float allPrices = 0;
    
    self.shopSelectArray = [[[[self.shopSelectArray rac_sequence] map:^id(NSNumber *value) {
        return @(isSelect);
    }] array] mutableCopy];
    
    self.cartData = [[[[self.cartData rac_sequence] map:^id(NSMutableArray *value) {
        return [[[[value rac_sequence] map:^id(DBCartModel *model) {
            [model setValue:@(isSelect) forKey:@"isSelect"];
            if (model.isSelect) {
                allPrices += model.p_quantity * model.p_price;
            }
            return model;
        }] array] mutableCopy];
    }]array] mutableCopy];
    self.allPrices = allPrices;
    [self.cartTableView reloadData];
}

- (void)rowSelect:(BOOL)isSelect IndexPath:(NSIndexPath *)indexPath {
    
    NSInteger section = indexPath.section;
    NSInteger row     = indexPath.row;
    
    NSMutableArray *goodsArray = self.cartData[section];
    NSInteger       shopCount  = goodsArray.count;
    DBCartModel    *model      = goodsArray[row];
    [model setValue:@(isSelect) forKey:@"isSelect"];
    
    //判断是否到达足够数量
    NSInteger isSelectShopCount = 0;
    for (DBCartModel *model in goodsArray) {
        if (model.isSelect) {
            isSelectShopCount++;
        }
    }
    [self.shopSelectArray replaceObjectAtIndex:section withObject:@(isSelectShopCount == shopCount ? YES : NO)];
    [self.cartTableView reloadSections:[NSIndexSet indexSetWithIndex:section] withRowAnimation:UITableViewRowAnimationNone];
    self.allPrices = [self getAllPrices];
}

- (void)rowChangQuantity:(NSInteger)quantity indexPath:(NSIndexPath *)indexPath {
    
    NSInteger section = indexPath.section;
    NSInteger row     = indexPath.row;
    
    DBCartModel *model = self.cartData[section][row];
    
    [model setValue:@(quantity) forKey:@"p_quantity"];
    
    [self.cartTableView reloadSections:[NSIndexSet indexSetWithIndex:section] withRowAnimation:UITableViewRowAnimationNone];
    
    self.allPrices = [self getAllPrices];
}

@end







