//
//  DBCartUIService.m
//  DBShopCartModule
//
//  Created by simon on 16/6/28.
//  Copyright © 2016年 DB. All rights reserved.
//

#import "DBCartUIService.h"
#import "DBCartViewModel.h"
#import "DBCartCell.h"
#import "DBCartHeaderView.h"
#import "DBCartFooterView.h"
#import "DBCartModel.h"
#import "DBNumberCount.h"

@implementation DBCartUIService

#pragma mark - UITableView Delegate/DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.viewModel.cartData.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.viewModel.cartData[section] count];
}

#pragma mark - header view
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return [DBCartHeaderView getCartHeaderHeight];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    NSMutableArray *shopArray = self.viewModel.cartData[section];
    
    DBCartHeaderView *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"DBCartHeaderView"];
    
    headerView.shopName = @"test shop name";
    
    //店铺全选
    [[[headerView.selectStoreGoodsButton rac_signalForControlEvents:UIControlEventTouchUpInside] takeUntil:headerView.rac_prepareForReuseSignal] subscribeNext:^(UIButton *x) {
        x.selected = !x.selected;
        BOOL isSelect = x.selected;
        [self.viewModel.shopSelectArray replaceObjectAtIndex:section withObject:@(isSelect)];
        for (DBCartModel *model in shopArray) {
            [model setValue:@(isSelect) forKey:@"isSelect"];
        }
        [self.viewModel.cartTableView reloadSections:[NSIndexSet indexSetWithIndex:section] withRowAnimation:UITableViewRowAnimationNone];;
        self.viewModel.allPrices = [self.viewModel getAllPrices];
    }];
    
    //店铺选中
    headerView.selectStoreGoodsButton.selected  = [self.viewModel.shopSelectArray[section] boolValue];
    
    return headerView;
}

#pragma mark - footer view
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return [DBCartFooterView getCartFooterHeight];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {

    NSMutableArray *shopArray = self.viewModel.cartData[section];
    
    DBCartFooterView *footerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"DBCartFooterView"];
    
    footerView.shopGoodsArray = shopArray;
    
    return footerView;
}

#pragma mark - cell
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [DBCartCell getCartCellHeight];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    DBCartCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DBCartCell" forIndexPath:indexPath];
    
    [self configCell:cell forRowAtIndexPath:indexPath];
    
    return cell;
    
}

- (void)configCell:(DBCartCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSInteger   section  = indexPath.section;
    NSInteger   row      = indexPath.row;
    DBCartModel *model   = self.viewModel.cartData[section][row];
    
    //cell 选中
    WEAK
    [[[cell.selectShopGoodsButton rac_signalForControlEvents:UIControlEventTouchUpInside] takeUntil:cell.rac_prepareForReuseSignal] subscribeNext:^(UIButton *x) {
        STRONG
        x.selected = !x.selected;
        [self.viewModel rowSelect:x.selected IndexPath:indexPath];
    }];
    //数量改变
    cell.numberCount.numberChangeBlock = ^(NSInteger changeCount) {
        STRONG
        [self.viewModel rowChangQuantity:changeCount indexPath:indexPath];
    };
    cell.model = model;
}


@end



