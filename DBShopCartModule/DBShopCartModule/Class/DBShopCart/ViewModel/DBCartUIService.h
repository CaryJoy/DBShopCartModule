//
//  DBCartUIService.h
//  DBShopCartModule
//
//  Created by simon on 16/6/28.
//  Copyright © 2016年 DB. All rights reserved.
//

#import <Foundation/Foundation.h>

@class DBCartViewModel;

@interface DBCartUIService : NSObject<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) DBCartViewModel *viewModel;

@end
