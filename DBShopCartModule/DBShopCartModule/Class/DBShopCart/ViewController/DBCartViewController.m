//
//  DBCartViewController.m
//  DBShopCartModule
//
//  Created by simon on 16/6/28.
//  Copyright © 2016年 DB. All rights reserved.
//

#import "DBCartViewController.h"
#import "DBCartUIService.h"
#import "DBCartViewModel.h"
#import "DBCartBar.h"

@interface DBCartViewController ()

@property (nonatomic, strong) DBCartUIService *service;

@property (nonatomic, strong) DBCartViewModel *viewModel;

@property (nonatomic, strong) UITableView     *cartTableView;

@property (nonatomic, strong) DBCartBar       *cartBar;

@end

@implementation DBCartViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.viewModel getData];
    [self.cartTableView reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = YES;
    self.title = @"购物车";
    [self.view addSubview:self.cartTableView];
    [self.view addSubview:self.cartBar];
    
    /*  RAC  */
    //全选
    [[self.cartBar.selectAllButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(UIButton *x) {
        x.selected = !x.selected;
        [self.viewModel selectAll:x.selected];
    }];
    //删除
    [[self.cartBar.deleteButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(UIButton *x) {
        
    }];
    //结算
    [[self.cartBar.balanceButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(UIButton *x) {
        NSLog(@"%@",self.viewModel.shopSelectArray);
    }];
    /* 观察价格属性 */
    WEAK
    [RACObserve(self.viewModel, allPrices) subscribeNext:^(NSNumber *x) {
       STRONG
        self.cartBar.money = x.floatValue;
    }];
    /* 全选 状态 */
    RAC(self.cartBar.selectAllButton, selected) = RACObserve(self.viewModel, isSelectAll);
}

#pragma mark - lazy load

- (DBCartUIService *)service {
	if(_service == nil) {
		_service = [[DBCartUIService alloc] init];
        _service.viewModel = self.viewModel;
	}
	return _service;
}

- (DBCartViewModel *)viewModel {
	if(_viewModel == nil) {
		_viewModel = [[DBCartViewModel alloc] init];
        _viewModel.cartVC = self;
        _viewModel.cartTableView = self.cartTableView;
	}
	return _viewModel;
}

- (UITableView *)cartTableView {
	if(_cartTableView == nil) {
		_cartTableView = [[UITableView alloc] initWithFrame:self.view.frame style:UITableViewStyleGrouped];
        [_cartTableView registerNib:[UINib nibWithNibName:@"DBCartCell" bundle:nil] forCellReuseIdentifier:@"DBCartCell"];
        [_cartTableView registerClass:NSClassFromString(@"DBCartFooterView") forHeaderFooterViewReuseIdentifier:@"DBCartFooterView"];
        [_cartTableView registerClass:NSClassFromString(@"DBCartHeaderView") forHeaderFooterViewReuseIdentifier:@"DBCartHeaderView"];
        _cartTableView.dataSource      = self.service;
        _cartTableView.delegate        = self.service;
        _cartTableView.backgroundColor = DBColor(243, 243, 243, 1);
        _cartTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, DBWindowWidth, 50)];
    }
	return _cartTableView;
}

- (DBCartBar *)cartBar {
	if(_cartBar == nil) {
		_cartBar = [[DBCartBar alloc] initWithFrame:CGRectMake(0, DBWindowHeight - 50, DBWindowWidth, 50)];
	}
	return _cartBar;
}

@end
