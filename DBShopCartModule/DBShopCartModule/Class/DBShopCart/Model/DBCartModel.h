//
//  DBCartModel.h
//  DBShopCartModule
//
//  Created by simon on 16/6/28.
//  Copyright © 2016年 DB. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DBCartModel : NSObject

@property (nonatomic, strong) NSString *p_id;

@property (nonatomic, assign) float p_price;

@property (nonatomic, strong) NSString *p_name;

@property (nonatomic, strong) NSString *p_imageUrl;

@property (nonatomic, assign) NSInteger p_stock;

@property (nonatomic, assign) NSInteger p_quantity;

//商品是否被选中
@property (nonatomic, assign) BOOL isSelect;

@end
